#!/usr/bin/bash

BUILD_DIR=cache
#ACC_HOST='cwe-513-vol956.cern.ch:'
#RSYNC_OPTS='-z'
CONTAINER_CMD='podman'

# Get dependencies from /acc
function fetch_dependency()
{
    local dep_path="$1"
    local destination="${BUILD_DIR}${dep_path}"
    if [[ -e "${destination}" ]]
    then
        echo "'${destination}' exists, skipping retrieval of '${dep_path}'"
    else
        echo "Copying '${dep_path}' to '${destination}'"
        mkdir -p $(dirname ${destination})
        rsync -a ${RSYNC_OPTS} ${ACC_HOST}${dep_path} $(dirname ${destination})
        # Resolve & retrieve symbolic links, for e.g. in accsoft-ds-fwk or cmw-fwk
        find "${destination}" -type l | while read symlink
        do
            local sub_dependency=$(readlink ${symlink})
            echo "Resolving ${symlink} as ${sub_dependency}"
            fetch_dependency ${sub_dependency}
        done
    fi
}

function fetch_deps() {
    fetch_dependency /acc/local/share/fesa/fesa-makefile/3.3.1
    # extend the use of the image to build accsoft-ds-server:
    fetch_dependency /acc/local/L867/accsoft/accsoft-ds-model/2.0.0
    fetch_dependency /acc/local/L867/cmw/cmw-log/4.0.0
    fetch_dependency /acc/local/L867/cmw/cmw-util/5.0.0
    # back to fesa deps:
    fetch_dependency /acc/local/L867/accsoft/accsoft-ds-fwk/DEV
    fetch_dependency /acc/local/L867/cmw/cmw-rda3/4.0.0
}

fetch_deps
IMAGE_NAME=fesa-builder
${CONTAINER_CMD} build --no-cache -t ${IMAGE_NAME} -f Containerfile.local ${BUILD_DIR}
REMOTE_IMAGE_NAME=gitlab-registry.cern.ch/acc-co/fesa/fwk/fesa-core/${IMAGE_NAME}:handmade
# Tag the built image to be able to push it
#${CONTAINER_CMD} tag ${IMAGE_NAME} ${REMOTE_IMAGE_NAME}
# Push the image to remote
#${CONTAINER_CMD} push ${REMOTE_IMAGE_NAME}
