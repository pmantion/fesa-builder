# fesa-builder: Container image to build & unittest fesa-core on gitlab-ci

## Build on gitlab-ci:

See `Containerfile.gitlab` and `container_install.sh`

## Build on a VM

See the contents of `build.sh` and `Containerfile.local`

Build the image: `./build.sh`
Run: `sudo podman run -ti -v $(pwd)/fesa-core:/workspace fesa-builder`
