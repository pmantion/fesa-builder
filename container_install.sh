#!/usr/bin/env bash
set -eux

yum -y install boost-devel openssl-devel libxml2-devel clang
yum clean all --enablerepo=*

# fetch files from /acc/local
fetch-acc-local L867/3rdparty/boost/1.69.0/
fetch-acc-local share/fesa/fesa-makefile/3.3.1/
fetch-acc-local L867/accsoft/accsoft-ds-fwk/2.0.1/
fetch-acc-local L867/cmw/cmw-rda3/4.0.0/
